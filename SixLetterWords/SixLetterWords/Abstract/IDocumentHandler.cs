﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SixLetterWords.Abstract {
  public interface IDocumentHandler {
    void Handle(IList<string> srcArray, int maxLength, IDocumentSaver saver);
  }
}
