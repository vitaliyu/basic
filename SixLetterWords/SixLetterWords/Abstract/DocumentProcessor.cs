﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SixLetterWords.Abstract {
  public abstract class DocumentProcessor {
    public void Process() {
      Validate();
      var data = Read();
      Handle(data);
      Save();
    }
    protected abstract void Validate();
    protected abstract IList<string> Read();
    protected abstract void Handle(IList<string> data);
    protected abstract void Save();
  }
}
