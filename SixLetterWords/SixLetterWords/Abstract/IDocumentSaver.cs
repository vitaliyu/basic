﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SixLetterWords.Abstract {
  public interface IDocumentSaver {
    void Save();
    void AddCombination(string[] found);
  }
}
