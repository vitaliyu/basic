﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SixLetterWords.Concrete.Txt;
using SixLetterWords.Settings;

namespace SixLetterWords {

  class Program {

    static void Main(string[] args) {

      AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;

      string filePath = Path.Combine(Path.GetFullPath(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "..\\..\\")), ConstantStorage.FilePath);

      var processor = new TxtProcessor(
         new TxtReader(filePath),
         new TxtSaver(filePath),
         new TxtValidator(filePath, ConstantStorage.MaxFileSize),
         new TxtHandler(),
         ConstantStorage.MaxCharactersLength);

      processor.Process();

    }

    public static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e) {
      Console.WriteLine(e.ExceptionObject);
      Environment.Exit(1);
    }

  }
}
