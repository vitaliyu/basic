﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SixLetterWords.Abstract;

namespace SixLetterWords.Concrete.Txt {
  public class TxtReader : IDocumentReader {
    private readonly string _path;
    public TxtReader(string path) {
      _path = path;
    }
    public IList<string> Read() {
      var result = new List<string>();
      try {
        foreach (var line in File.ReadAllLines(_path)) {
          result.AddRange(line.Split(new char[0])); //split by whitespaces if such exists                                   
        }

      }
      catch (Exception e) {
        Console.WriteLine("The file could not be read due to an error");
        Environment.Exit(1);
      }
      return result;
    }
  }
}
