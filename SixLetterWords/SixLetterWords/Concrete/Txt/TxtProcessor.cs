﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SixLetterWords.Abstract;

namespace SixLetterWords.Concrete.Txt {
  public class TxtProcessor: DocumentProcessor {
    private IDocumentReader _reader;
    private IDocumentSaver _saver;
    private IDocumentValidator _validator;
    private IDocumentHandler _processor;
    private readonly int _maxLength;

    public TxtProcessor(IDocumentReader reader, IDocumentSaver saver, IDocumentValidator validator, IDocumentHandler processor, int maxLength) {
      _reader = reader;
      _saver = saver;
      _validator = validator;
      _processor = processor;
      _maxLength = maxLength;
    }
    protected override void Handle(IList<string> srcArray) {
      _processor.Handle(srcArray, _maxLength, _saver);
    }

    protected override IList<string> Read() {
      return _reader.Read();
    }

    protected override void Save() {
      _saver.Save();
    }

    protected override void Validate() {
      _validator.Validate();
    }
  }
}
