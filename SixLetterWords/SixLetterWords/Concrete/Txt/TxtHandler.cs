﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SixLetterWords.Abstract;

namespace SixLetterWords.Concrete.Txt {
  public class TxtHandler : IDocumentHandler {
    public void Handle(IList<string> srcArray, int maxLength, IDocumentSaver saver) {
      srcArray = srcArray
          .Where(x => x.Length <= maxLength)
          .ToList(); //remove too long strings 
      CalculateSummaryRecursive(srcArray, maxLength, new List<string>(), saver);
    }

    private static void CalculateSummaryRecursive(IList<string> srcArray, int maxLength, IList<string> partial, IDocumentSaver dataSaver) {
      int sum = 0;
      foreach (var str in partial) sum += str.Length;

      if (sum == maxLength) {
        dataSaver.AddCombination(partial.ToArray());
      }

      if (sum >= maxLength)
        return;

      for (var i = 0; i < srcArray.Count; i++) {
        var remaining = new List<string>();
        var next = srcArray[i];
        for (var j = i + 1; j < srcArray.Count; j++) remaining.Add(srcArray[j]);

        var partialRecursive = new List<string>(partial);
        partialRecursive.Add(next);
        CalculateSummaryRecursive(remaining, maxLength, partialRecursive, dataSaver);
      }
    }
  }
}
