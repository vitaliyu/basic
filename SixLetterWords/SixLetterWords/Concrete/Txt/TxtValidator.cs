﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SixLetterWords.Abstract;

namespace SixLetterWords.Concrete.Txt {
  public class TxtValidator : IDocumentValidator {
    private readonly string _path;
    int _maxFileSize;
    public TxtValidator(string path, int maxFileSize) {
      _path = path;
      _maxFileSize = maxFileSize;
    }
    public void Validate() {
      if (!File.Exists(_path)) {
        Console.WriteLine("input file not found");
        Environment.Exit(1);
      }

      var length = new FileInfo(_path).Length;
      if (length > _maxFileSize) {
        Console.WriteLine("input file size is too high");
        Environment.Exit(1);
      }

    }
  }
}
