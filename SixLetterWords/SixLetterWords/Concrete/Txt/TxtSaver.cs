﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SixLetterWords.Abstract;

namespace SixLetterWords.Concrete.Txt {
  public class TxtSaver : IDocumentSaver {
    private readonly string _path;
    List<string> _scope = new List<string>();
    public TxtSaver(string path) {
      _path = path;
      //initial newline
      _scope.Add(string.Empty);
    }

    public void AddCombination(string[] found) {
      _scope.Add($"{string.Join("+", found)}={found.Aggregate((a, b) => a + b)}");
      //memory optimization
      if (_scope.Count > 1000) {
        Save();
      }
    }

    public void Save() {
      try {
        using (var w = File.AppendText(_path)) {
          foreach (var item in _scope) {
            w.WriteLine(item);
          }
        }

      }
      catch (Exception e) {
        Console.WriteLine("The file could not be written due to an error");
        Environment.Exit(1);
      }
      finally {
        _scope.Clear();
      }
    }
  }
}
