﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SixLetterWords.Settings 
{
  public class ConstantStorage 
  {
    public static int MaxCharactersLength => 6;

    public static string FilePath => "input.txt";

    public static int MaxFileSize => 1024 * 1024;  //1MB
  }
}
